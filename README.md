# Solution Architect Associate-prep

Этот проект создан в рамках подготовки к сертификации [**AWS Soultion Architect Associate**](https://aws.amazon.com/certification/certified-solutions-architect-associate/)
Задания выполнялись по мере прохождения [бесплатного онлайн-тренинга](https://www.aws.training/Details/Curriculum?id=20685)

Здесь будут публиковаться мои находки по автоматизации создания инфраструктуры и другая информация

1. раздел _0-Setup environment_ описывает, как создать контейнер Docker для всех дальнейших упраженений

2. раздел _1-Basic compute services_ содержит примеры создания простых инфраструктур EC2
    - _11-ec2 virtual machine_ - создаем Web-сервер в изолированной Security group
    - _12-wordpress server_ - полностью автоматическая инсталляция сервера Wordpress с БД в RDS и двумя security group внешней и внутренней
    - _13-dns_configuration_ - создание трех серверов в трех регионах, S3 папки для сайта и записи в DNS
    - _14-vpc_config_ - создание VPC с четырьмя подсетями, IGW, маршрутом наружу и bastion host

### Полезные ссылки
[Как написать хороший README](https://medium.com/astrolabe/%D0%BA%D0%B0%D0%BA-%D0%BD%D0%B0%D0%BF%D0%B8%D1%81%D0%B0%D1%82%D1%8C-%D1%85%D0%BE%D1%80%D0%BE%D1%88%D0%B8%D0%B9-readme-159f88076b26)