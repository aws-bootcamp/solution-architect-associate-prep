# 0-Setup environment 
Здесь я хочу создать рабочее окружение для всех дальнейших опытов с AWS

**Основные задачи данного проекта:**
- подготовить рабочую среду для дальнейших экспериментов с консолью AWS и Python API
- освежить знание Docker
- подготовить окружение для работы с AWS через Terraform

# Работа с проектом
Клонируем проект и собираем контейнер
```
git clone https://gitlab.com/aws-bootcamp/solution-architect-associate-prep.git
cd solution-architect-associate-prep/0-Setup \environment
bash makeContainer.sh
```

### Перед использованием нужно настроить переменные окружения с реквизитами доступа к управляющей роли AWS
- Сначала создаем IAM роль `operator` как описано тут
  https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-creds
- затем создаем копию файла `aws-env-list_template` и прописываем в нее параметры доступа и регион
  ```
  cp aws-env-list_template aws-env-list && vim aws-env-list
  ```  
- Вторым этапом нужно сгенерировать ключ доступа к виртуальным машинам по ssh
  - Процесс создания ключей доступа через **aws cli** описан здесь:
https://docs.aws.amazon.com/cli/latest/userguide/cli-services-ec2-keypairs.html

  - Также пару ключей SSH для доступа к виртуальной машине можно создать через консоль **EC2**.

### Собранный контейнер можно использовать разными способами:

### 1. Работа в консоли через _shell_
```
docker run -ti --name aws-terraform aws-terraform /bin/bash
```
Далее, например, для создания пары ключей можно выполнить
```
aws ec2 create-key-pair --key-name aws-console-key --query 'KeyMaterial' --output text > aws-console-key.pem
```
далее можно на основной рабочей консоли машины скопировать ключ в локальную папку
```
docker cp aws-terraform:/home/awssetup/aws-console-key.pem .
chmod 400 aws-console-key.pem
```
и после создания виртуальной машины подключить к ней
```
ssh -i aws-console-key.pem ubuntu@<public IP>
```
### 2. Работа через Jupyter notebook
```
docker run --name terraform-test -p 8888:8888 -v "$(pwd)":/home/operator/tf aws-terraform sh -c "jupyter notebook --allow-root --no-browser --ip=0.0.0.0 --port=8888”
```
После этого любым браузером подключаемся к localhost:8888 с ключом доступа, который будет указан в консоли после запуска контейнера

### 3. Работа через IDE, поддерживающее запуск кода в контейнере, в моем случае **PyCharm**
Подключаем Docker контейнер aws-terraform в качестве среды исполнения для кода.
После этого можно использовать API Python

### Пример использования готового контейнера
В консольном режиме создаем образ контейнера через terraform

#### 1. Собираем контейнер
```
bash make_container.sh
```
#### 2. Создаем IAM роль `operator` как описано тут
  https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-creds 
    И копируем реквизиты доступа

#### 3. Создаем файл aws-env-list
```
cp aws-env-list_template aws-env-list && vim aws-env-list
```
И прописываем в него ключи доступа к API AWS и рабочий регион

#### 4. Запускаем контейнер с примонтированной локальной папкой
```
docker run -ti --name aws-terraform-1 --env-file aws-env-list -v "$(pwd)":/home/operator/tf aws-terraform /bin/bash
```
#### 5. Создаем пару SSH ключей для доступа к виртуальной машине
```
aws ec2 create-key-pair --key-name aws-console-key --query 'KeyMaterial' --output text > aws-console-key.pem
```
_операторские ключи доступа берутся из переменных окружения контейнера_

#### 6. Создаем файл для terraform
`vim example.tf`
____
```
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}


resource "aws_instance" "tf-example" {
  ami           = "ami-04137ed1a354f54c4"
  instance_type = "t2.micro"
  key_name = "aws-console-key"
}
```

#### 7. Работа с terraform
Пример работы с _terraform_ [описан тут](https://learn.hashicorp.com/tutorials/terraform/aws-build?in=terraform/aws-get-started)

```
terraform init
terraform validate
terraform plan
terraform apply
terraform state
terraform show
```
В выводе последней команды находим публичный IP вновь созданного сервера
```
ssh -i aws-console-key.pem ubuntu@IP
```

#### 8. Очистка окружения
```
terraform destroy
```

### Полезные ссылки
[Как писать README - файлы ](https://docs.github.com/en/free-pro-team@latest/github/writing-on-github/basic-writing-and-formatting-syntax)
