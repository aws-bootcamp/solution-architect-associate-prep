terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}


resource "aws_instance" "tf-example" {
  ami           = "ami-04137ed1a354f54c4"
  instance_type = "t2.micro"
  key_name = "tf-example-key"
}

resource "aws_instance" "london" {
  ami           = "ami-08d70e59c07c61a3a"
  instance_type = "t2.micro"
}