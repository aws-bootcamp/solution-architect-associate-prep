variable "region" {}
variable "amis" {}
variable "instance_type" {}
variable "project_name" {}
variable "environment" {}
variable "ssh_security_key" {}
