#### глобальная часть

#### создание ресурсов
# Создается сервер
# на него с помощью used_data устанавливается Apache2 и создается приветственная страница
# на выходе IP-адрес, по которому к серверу можно обратиться
# пример отсюда https://www.bogotobogo.com/DevOps/Terraform/Terraform-terraform-userdata.php
# добавил создание security group "my_WEB"
# ВАЖНО: для того, чтобы весь софт мог установиться в security group должен быть разрешен исходящий трафик в Интернет

resource "aws_instance" "docker_host" {
  ami           = var.amis[var.region]
  instance_type = var.instance_type
  tags = {
    Name = "test-docker-server"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
  }
  key_name = var.ssh_security_key

  # Script to install Docker
  user_data = templatefile( "./modules/setup-host/install_docker.sh", { } )
  security_groups = [ "my_WEB" ]

}

resource "aws_security_group" "my_WEB" {
  name        = "my_WEB"
  description = "Allow HTTP inbound traffic"

  ingress {
    description = "Allow all HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow all SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "my_WEB"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
 }
}

output "public_ip" { value = aws_instance.docker_host.public_ip}
output "public_dns" { value = aws_instance.docker_host.public_dns}
output "availability_zone" { value = aws_instance.docker_host.availability_zone}