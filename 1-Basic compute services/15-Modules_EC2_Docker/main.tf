terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
    docker = {
      source = "terraform-providers/docker"
    }
  }
}


provider "aws" {
  profile = "default"
  region  = var.region
}

provider "docker" {
    #alias = "docker"
    #host         = "ssh://ubuntu@${module.setup-host.public_ip}:22"
    host         = "ssh://ubuntu@8.8.8.8:22"

    #cert_material = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCflYYaVOn0PUAQYvd3D08UafXwpu3/7W0+0b5uO/idj/QwVd+M1h3EiC47CRGyNHITxRdD0jJERq+v1Bk+MCdAQTgzWHYG+yo0tBBMycQ2Dy6B3JuRX9MankIaGBBhFx/1nnZWLj5M5E9143RyHoZX/vUwdtjb3MNpfK8JdRRWhQlUvn9yneybPhykE4xxzUk58iNyIYA8h0zDoJ5p829jlMCjJ8vGY+M3NMtwEPu8dqldqS1zc10nwuP9NcikyJu9EldtTyZjYwXi8A5NSISHLrUgFuUMRCczp0IhAWtA5gUm9cj2SPOo7YCLRgq7KA/bfPgVngfG4AQBWPdGiotj"
    #key_material = file("aws-console-key.pem")

    #cert_material = "./ca.pem"
    #key_material = "./ca-key.pem"
}



module "setup-host" {
    source   = "./modules/setup-host"

    region = var.region
    amis = var.amis
    instance_type = var.instance_type
    project_name = var.project_name
    environment = var.environment
    ssh_security_key = var.ssh_security_key
}

module "setup-docker" {
  #provider   = docker
  depends_on = [module.setup-host]
  source     = "./modules/setup-docker"
}


#### переменные для вывода результатов

output "ip" { value = module.setup-host.public_ip }
output "dns" { value = module.setup-host.public_dns }
output "availability_zone" { value = module.setup-host.availability_zone }