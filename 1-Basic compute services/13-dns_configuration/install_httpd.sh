#! /bin/bash
sudo apt-get update
sudo apt-get install -y apache2
cd /var/www/html

# Скачиваем заглавную страницу сервера
sudo curl -O "http://${bucket}/${file}"

###echo "<html><h2>--- WELCOME_MESSAGE ---</h2></html>" > index.html

# меняем WELCOME_MESSAGE на сообщение сервера
sudo sed -i "s/WELCOME_MESSAGE/${greeting}/" index.html

# запускаем
sudo systemctl start apache2
sudo systemctl enable apache2

