# Переменные загружаются из файла "terraform.tfvars"
# Общаие параметры проекта
variable "project_name" { type = string }
variable "environment" { type = string }
variable "instance_type" { type = string }

# регионы, в которых будут установлены серверы
variable "regions" { default = ["eu-west-1", "ap-southeast-1", "us-east-1"] }

# Приветствие сервера
variable "eu_greeting" { default = "Welcome from cloudy Ireland server !!!" }
variable "asia_greeting" { default = "Welcome from equatorial Singapore server !!!" }
variable "usa_greeting" { default = "Welcome from old good American server !!!" }

# Образы серверов
variable "amis" {
  type = map
  default = {
    "eu-west-1" = "ami-04137ed1a354f54c4"
    "eu-west-2" = "ami-05c424d59413a2876"
    "ap-southeast-1" = "ami-093da183b859d5a4b"
    "us-east-1" = "ami-0dba2cb6798deb6d8"
  }
}

variable "ssh_security_key" {
    default = "aws-console-key"
}

# HTML file name
variable "index_filename" { default = "index.html" }

