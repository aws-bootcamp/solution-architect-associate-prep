#### глобальная часть
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

# Создание провайдеров для разных регионов
# Пример отсюда
# https://www.terraform.io/docs/configuration/providers.html
# поиск AMI для разных провадеров
# https://www.terraform.io/docs/configuration/data-sources.html
provider "aws" {
  profile = "default"
  region  = var.regions[0]    # Ireland
}

provider "aws" {
  alias = "asia"
  region = var.regions[1]    # Singapore
}

provider "aws" {
  alias = "usa"
  region = var.regions[2]    # N. Virginia
}


#### создание ресурсов
# - Выше созданы три провайдера для регионов Ирландия, Сингапур и Вирджиния
# Если нужен доступ по SSH к серверам, то нужно создать в каждом регионе SSH keypair, публичный ключ можно импортировать
# Создать публичный ключ из секретного - ssh-keygen -y -f aws-console-key.pem > aws-console-key.pub
# Затем в каждом регионе создаем Security Group с доступом к портам 80 и (в целях отладки) SSH
# Затем создаем серверы в каждом из регионов и выводим их параметры
#


### Import public keys to create servers
# Hint: create public key from private
# ssh-keygen -y -f aws-console-key.pem > aws-console-key.pub

resource "aws_key_pair" "asia-key" {
  provider = aws.usa
  key_name   = var.ssh_security_key
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCflYYaVOn0PUAQYvd3D08UafXwpu3/7W0+0b5uO/idj/QwVd+M1h3EiC47CRGyNHITxRdD0jJERq+v1Bk+MCdAQTgzWHYG+yo0tBBMycQ2Dy6B3JuRX9MankIaGBBhFx/1nnZWLj5M5E9143RyHoZX/vUwdtjb3MNpfK8JdRRWhQlUvn9yneybPhykE4xxzUk58iNyIYA8h0zDoJ5p829jlMCjJ8vGY+M3NMtwEPu8dqldqS1zc10nwuP9NcikyJu9EldtTyZjYwXi8A5NSISHLrUgFuUMRCczp0IhAWtA5gUm9cj2SPOo7YCLRgq7KA/bfPgVngfG4AQBWPdGiotj"
}

resource "aws_key_pair" "usa-key" {
  provider = aws.asia
  key_name   = var.ssh_security_key
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCflYYaVOn0PUAQYvd3D08UafXwpu3/7W0+0b5uO/idj/QwVd+M1h3EiC47CRGyNHITxRdD0jJERq+v1Bk+MCdAQTgzWHYG+yo0tBBMycQ2Dy6B3JuRX9MankIaGBBhFx/1nnZWLj5M5E9143RyHoZX/vUwdtjb3MNpfK8JdRRWhQlUvn9yneybPhykE4xxzUk58iNyIYA8h0zDoJ5p829jlMCjJ8vGY+M3NMtwEPu8dqldqS1zc10nwuP9NcikyJu9EldtTyZjYwXi8A5NSISHLrUgFuUMRCczp0IhAWtA5gUm9cj2SPOo7YCLRgq7KA/bfPgVngfG4AQBWPdGiotj"
}


# Создаем серверы

# Группа безопасности и сервер для Ирландии
resource "aws_security_group" "my_WEB-eu" {
  name        = "my_WEB-eu"
  description = "Allow HTTP inbound traffic"

  ingress {
    description = "Allow all HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Для отладочного доступа к серверу по SSH убрать комментарии
    ingress {
    description = "Allow all SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "eu_web_server" {
  ami           = var.amis[var.regions[0]]  # Take snapshot from dictionary
  instance_type = var.instance_type
  tags = {
    Name = "eu-web-server"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
  }
  key_name = var.ssh_security_key
  user_data = templatefile( "install_httpd.sh", { greeting = var.eu_greeting
                                                  bucket = aws_s3_bucket.s3_bucket.bucket_regional_domain_name,
                                                  file = var.index_filename } )
  security_groups = [ "my_WEB-eu" ]

}


# Группа безопасности и сервер для Америки

resource "aws_security_group" "my_WEB-us" {
  provider = aws.usa
  name        = "my_WEB-us"
  description = "Allow HTTP inbound traffic"

  ingress {
    description = "Allow all HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Для отладочного доступа к серверу по SSH убрать комментарии
    ingress {
    description = "Allow all SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "us_web_server" {
  provider = aws.usa
  ami           = var.amis[var.regions[2]]
  instance_type = var.instance_type
  tags = {
    Name = "us-web-server"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
  }
  key_name = var.ssh_security_key
  user_data = templatefile( "install_httpd.sh", { greeting = var.usa_greeting
                                                  bucket = aws_s3_bucket.s3_bucket.bucket_regional_domain_name,
                                                  file = var.index_filename } )
  security_groups = [ "my_WEB-us" ]

}

# Группа безопасности и сервер для Азии

resource "aws_security_group" "my_WEB-asia" {
  provider = aws.asia
  name        = "my_WEB-asia"
  description = "Allow HTTP inbound traffic"

  ingress {
    description = "Allow all HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Для отладочного доступа к серверу по SSH убрать комментарии
    ingress {
    description = "Allow all SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "asia_web_server" {
  provider = aws.asia
  ami           = var.amis[var.regions[1]]
  instance_type = var.instance_type
  tags = {
    Name = "asia-web-server"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
  }
  key_name = var.ssh_security_key
  user_data = templatefile( "install_httpd.sh", { greeting = var.asia_greeting,
                                                  bucket = aws_s3_bucket.s3_bucket.bucket_regional_domain_name,
                                                  file = var.index_filename } )
  security_groups = [ "my_WEB-asia" ]

}


#### переменные для вывода результатов
output "eu_ip" {   value = aws_instance.eu_web_server.public_ip }
output "eu_dns" {   value = aws_instance.eu_web_server.public_dns }

output "us_ip" {   value = aws_instance.us_web_server.public_ip}
output "us_dns" {   value = aws_instance.us_web_server.public_dns }

output "asia_ip" {  value = aws_instance.asia_web_server.public_ip }
output "asia_dns" {  value = aws_instance.asia_web_server.public_dns }

output "s3_bucket" { value = aws_s3_bucket.s3_bucket.bucket_regional_domain_name }
#output "s3_object" { value = aws_s3_bucket_object.index_page }

output "Route53_zone_id" { value = data.aws_route53_zone.selected.zone_id }