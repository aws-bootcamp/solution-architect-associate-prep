# Создаем публичный S3 bucket, загружаем в него файл с будущей страницей
# Attention: чтобы это работало, нужно роли operator добавить права на S3FullAccess
# TBD - сделать приватный, создать роль для разрешения доступа к репозиторию, назначать роль на серверы.


resource "aws_s3_bucket" "s3_bucket" {
  bucket = "awsbootcamp-test-page-0001"
  acl    = "public-read"

  tags = {
    Name = "S3 web page distribution bucket"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
  }
}

# Upload page to the bucket
resource "aws_s3_bucket_object" "index_page" {
  bucket = aws_s3_bucket.s3_bucket.bucket
  key    = var.index_filename
  source = "index_html_template.html"
  acl    = "public-read"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("index_html_template.html")
}

