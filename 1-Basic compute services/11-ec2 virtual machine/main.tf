#### глобальная часть
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

#### создание ресурсов
# Создается сервер
# на него с помощью used_data устанавливается Apache2 и создается приветственная страница
# на выходе IP-адрес, по которому к серверу можно обратиться
# пример отсюда https://www.bogotobogo.com/DevOps/Terraform/Terraform-terraform-userdata.php
# добавил создание security group "my_WEB"
# ВАЖНО: для того, чтобы весь софт мог установиться в security group должен быть разрешен исходящий трафик в Интернет

resource "aws_instance" "example" {
  ami           = var.amis[var.region]
  instance_type = var.instance_type
  tags = {
    Name = "test-web-server"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
  }
  key_name = var.ssh_security_key
  user_data = file("install_apache.sh")
  security_groups = [ "my_WEB" ]

}

resource "aws_security_group" "my_WEB" {
  name        = "my_WEB"
  description = "Allow HTTP inbound traffic"
#  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Allow all HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "my_WEB"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
 }
}

#### переменные для вывода результатов
resource "aws_eip" "ip" {
  vpc      = true
  instance = aws_instance.example.id
}

output "ip" {
  value = aws_eip.ip.public_ip
}

output "dns" {
  value = aws_eip.ip.public_dns
}

output "avzone" {
  value = aws_instance.example.availability_zone
}