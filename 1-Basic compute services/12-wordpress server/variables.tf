variable "region" {
  default = "eu-west-1"
}

variable "amis" {
  type = map
  default = {
    "eu-west-1" = "ami-04137ed1a354f54c4"
    "eu-west-2" = "ami-05c424d59413a2876"
  }
}

variable "ssh_security_key" {
    default = "aws-console-key"
}

# Переменные загружаются из файла "terraform.tfvars"
# Общаие параметры проекта
variable "project_name" { type = string }
variable "environment" { type = string }
variable "instance_type" { type = string }

# Параметры сервера СУБД
variable "db_name" { type = string }
variable "db_username" { type = string }
variable "db_password" { type = string }

