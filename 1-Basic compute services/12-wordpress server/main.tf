#### глобальная часть
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

#### создание ресурсов
# Создается СУБД mysql, для нее создается группа безопасности sc_db_access без доступа в Интернет
# для внешнего сервера создается security group "my_WEB" и прописывается доступ из нее к серверу СУБД
# Создается web-сервер
# на него с помощью used_data устанавливается wordpress и за счет файла шаблона с параметрами прописываются настройки
# на выходе IP-адрес, по которому к серверу можно обратиться
# пример отсюда https://www.bogotobogo.com/DevOps/Terraform/Terraform-terraform-userdata.php
# ВАЖНО: для того, чтобы весь софт мог установиться в security group "my_WEB" должен быть разрешен исходящий трафик в Интернет
# доступ к серверу по SSH для отладки

# База данных
# описание https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance
resource "aws_db_instance" "wordpress_db" {
  allocated_storage    = 5
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = var.db_name
  username             = var.db_username
  password             = var.db_password
  parameter_group_name = "default.mysql5.7"
  vpc_security_group_ids = [ aws_security_group.sc_db_access.id ]
  tags = {
    Name        = "my_Wordpress_db"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
    }
  skip_final_snapshot = "false"
  # если хотим чтобы создавался финальный снэпшот, то нужно задать его id
  final_snapshot_identifier = "wordpress-final-snapshot-${var.environment}"
}


resource "aws_security_group" "sc_db_access" {
  name        = "sc_db_access"
  description = "Allow internal Wordpress database access"
#  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Allow internal Wordpress database access"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = [ aws_security_group.my_WEB.id ]
  }

  tags = {
    Name        = "sc_db_access"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
 }
}

resource "aws_instance" "wordpress_web_server" {
  ami           = var.amis[var.region]
  instance_type = var.instance_type
  tags = {
    Name = "wordpress-server"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
  }
  key_name = var.ssh_security_key
  user_data = templatefile( "install_wordpress.sh", { user= var.db_username , pass= var.db_password , db= var.db_name , host= aws_db_instance.wordpress_db.endpoint } )
  security_groups = [ "my_WEB" ]

}

resource "aws_security_group" "my_WEB" {
  name        = "my_WEB"
  description = "Allow HTTP inbound traffic"
#  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Allow all HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Для отладочного доступа к серверу по SSH убрать комментарии
/*
    ingress {
    description = "Allow all SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
*/

   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "my_WEB"
    Terraform   = "true"
    Project     = var.project_name
    Environment = var.environment
 }
}

#### переменные для вывода результатов
resource "aws_eip" "ip" {
  vpc      = true
  instance = aws_instance.wordpress_web_server.id
}

output "ip" {
  value = aws_eip.ip.public_ip
}

output "dns" {
  value = aws_eip.ip.public_dns
}

output "avzone" {
  value = aws_instance.wordpress_web_server.availability_zone
}

#output "db_name" { value = var.db_name }
#output "db_username" { value = var.db_username }
#output "db_password" { value = var.db_password }
output "db_endpoint" { value = aws_db_instance.wordpress_db.endpoint }
