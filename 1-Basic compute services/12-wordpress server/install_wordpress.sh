#! /bin/bash
sudo apt-get update
sudo apt-get install -y apache2 php php-mysql
cd /var/www/html
echo "<html><h2> Installing a new shining Wordpress server ... </h2></html>" > index.html

# Скачиваем и устанавливаем Wordpress
wget https://wordpress.org/wordpress-5.1.1.tar.gz
tar -xzf wordpress-5.1.1.tar.gz
cp -r wordpress/* /var/www/html/
rm -rf wordpress
rm -rf wordpress-5.1.1.tar.gz
chmod -R 755 wp-content
chown -R www-data:www-data wp-content

# Копируем конфиг, а затем
# ищем строчки в конфигурационном файле и заменяем их на параметры подключения
# к серверу
cp wp-config-sample.php wp-config.php
sed -i "s/database_name_here/${db}/" wp-config.php
sed -i "s/username_here/${user}/" wp-config.php
sed -i "s/password_here/${pass}/" wp-config.php
sed -i "s/localhost/${host}/" wp-config.php

# альтернативный вариант - использовать wp-cli для создания конфиг файла из консоли
# https://make.wordpress.org/cli/handbook/guides/installing/
# https://developer.wordpress.org/cli/commands/config/create/
# https://make.wordpress.org/cli/handbook/guides/installing/ - установка

# запускаем
sudo systemctl start apache2
sudo systemctl enable apache2

# Удаляем index.html, чтобы читался index.php
rm index.html
