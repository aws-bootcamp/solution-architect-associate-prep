### Базовые вычислительные сервисы AWS

#### **11-ec2 virtual machine**
 - виртуальные машины EC2
 - переменные terraform для задания окружения проекта
 - вывод данных о инфраструктуре (Public IP, Public DNS) через переменные 
 - установка на сервер Apache2 и создание стартовой страницы
 - создание для сервера отдельной security group

#### **12-wordpress server**
Создание wordpress-инсталляции с СУБД на RDS
 - создание СУБД сервера в RDS
 - security group для обращения к этому серверу
 - создание ec2 машины и установка на нее wordpress c параметрами доступа к СУБД
 - создание для сервера отдельной security group c доступом из Интернет

#### **13-dns_configuration**
Создание записи DNS и трех серверов в разных регионах. Поиграться с разными типами **XXX**
 - для инсталляции главной страницы серверов создать S3 bucket, загрузить страницу на него и сделать общедоступной
 - создать три сервера в разных регионах, скриптом выкачать страницу и поменять приветствие на название сервера
 - создать запись DNS, которая будет балансировать нагрузку между этими серверами
 - можно поэкспериментировать с разными типами балансировки

#### **14-VPC configuration**
Создание VPC с установленным приложением

### Полезные ссылки

[Вводный курс **Terraform**](https://learn.hashicorp.com/tutorials/terraform/aws-build)
[Установка Apache2 на сервер через user_data](https://www.bogotobogo.com/DevOps/Terraform/Terraform-terraform-userdata.php)
_Примечание: в этом примере синтаксис загрузки файла скрипта устарел, в main.tf - современный вариант_

[Создание Security Group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group)

[Использование файлов шаблонов в user_data в terraform](https://www.terraform.io/docs/configuration/functions/templatefile.html)

[How to organize Terraform modules](https://blog.smartlogic.io/how-i-organize-terraform-modules-off-the-beaten-path/)
