#### глобальная часть
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

# Создается VPC
# две сети для публичного сегмента
# две сети для внутреннего сегмента
# Internet gateway
# bastion host в autoscaling group позади ELB
# DNS указывает на bastion host
# машина во внутреннем сегменте
# Доступ внутренней машины к S3 bucket, который создается и заполняется файлами
# Для доступа создается политика и роль

# Создание провайдера
provider "aws" {
  profile = "default"
  region  = var.region    # Ireland
}

#### создание ресурсов
# Если нужен доступ по SSH к серверам, то нужно создать в каждом регионе SSH keypair, публичный ключ можно импортировать
# Создать публичный ключ из секретного - ssh-keygen -y -f aws-console-key.pem > aws-console-key.pub
#

### Import public keys to create servers
# Hint: create public key from private
# ssh-keygen -y -f aws-console-key.pem > aws-console-key.pub

#resource "aws_key_pair" "asia-key" {
#  provider = aws.usa
#  key_name   = var.ssh_security_key
#  public_key = var.ssh_public_key
#}

# Создаем VPC

resource "aws_vpc" "main_vpc" {
  cidr_block       = "10.5.0.0/16"
  instance_tenancy = "default"
  enable_dns_hostnames = "true"

  # This configuration combines some "default" tags with optionally provided additional tags
  tags = merge( local.project_tags, { Name = "Main-VPC" }, )

}

# Создаем подсети
resource "aws_subnet" "snet-public-1" {
  vpc_id     = aws_vpc.main_vpc.id
  cidr_block = "10.5.0.0/24"
  availability_zone = var.az[0]

  map_public_ip_on_launch = "true"

  tags = merge( local.project_tags, { Name = "sn-public-1" }, )
  }

resource "aws_subnet" "snet-public-2" {
  vpc_id     = aws_vpc.main_vpc.id
  cidr_block = "10.5.1.0/24"
  availability_zone = var.az[1]

  map_public_ip_on_launch = "true"

  tags = merge( local.project_tags, { Name = "sn-public-2" }, )
  }

resource "aws_subnet" "snet-private-1" {
  vpc_id     = aws_vpc.main_vpc.id
  cidr_block = "10.5.10.0/24"
  availability_zone = var.az[0]

  tags = merge( local.project_tags, { Name = "sn-private-1" }, )

  }

resource "aws_subnet" "snet-private-2" {
  vpc_id     = aws_vpc.main_vpc.id
  cidr_block = "10.5.11.0/24"
  availability_zone = var.az[1]

  tags = merge( local.project_tags, { Name = "sn-private-2" }, )
  }

# Internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = merge( local.project_tags, { Name = "vpc-igw-1" }, )
}

resource "aws_default_route_table" "r" {
  default_route_table_id = aws_vpc.main_vpc.default_route_table_id

  route {
    cidr_block    = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id

  }

  tags = {
    Name = "default table"
  }
}


# Группа безопасности и сервер для Ирландии
resource "aws_security_group" "my_WEB-eu" {
  name        = "my_WEB-eu"
  description = "Allow HTTP&SSH inbound traffic"
  vpc_id = aws_vpc.main_vpc.id
  tags = merge( local.project_tags, { Name = "sg-web-1" }, )

  ingress {
    description = "Allow all HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Для отладочного доступа к серверу по SSH убрать комментарии
    ingress {
    description = "Allow all SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

# Группа безопасности внутренняя
resource "aws_security_group" "my_private-eu" {
  name        = "my_private-eu"
  description = "Allow HTTP&SSH inbound traffic from public subnets"
  vpc_id = aws_vpc.main_vpc.id
  tags = merge( local.project_tags, { Name = "sg-private-1" }, )

  ingress {
    description = "Allow all HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["10.5.0.0/24", "10.5.1.0/24"]
  }

# Для отладочного доступа к серверу по SSH убрать комментарии
    ingress {
    description = "Allow all SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.5.0.0/24", "10.5.1.0/24"]
  }

   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.5.0.0/24", "10.5.1.0/24"]
  }

}


# Создаем серверы
# хост для доступа к инфраструктуре
resource "aws_instance" "bastion-host-1" {
  ami           = var.amis[var.region]  # Take snapshot from dictionary
  instance_type = var.instance_type
  tags = merge( local.project_tags, { Name = "bastion-host-1" }, )
  subnet_id = aws_subnet.snet-public-1.id
  availability_zone = var.az[0]

  key_name = var.ssh_security_key
  user_data = templatefile( "install_apache.sh", { greeting = var.eu_greeting })
  vpc_security_group_ids = [ aws_security_group.my_WEB-eu.id ]
  depends_on = [aws_internet_gateway.gw]
}



#### переменные для вывода результатов
output "eu_ip" {   value = aws_instance.bastion-host-1.public_ip }
output "eu_dns" {   value = aws_instance.bastion-host-1.public_dns }

#output "s3_bucket" { value = aws_s3_bucket.s3_bucket.bucket_regional_domain_name }
#output "s3_object" { value = aws_s3_bucket_object.index_page }

#output "Route53_zone_id" { value = data.aws_route53_zone.selected.zone_id }