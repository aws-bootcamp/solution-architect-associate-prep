# Route53 - config

/*
# как получить ID зоны
data "aws_route53_zone" "selected" {
  name         = "awsbootcamp.online."
  private_zone = false
}

# Set up DNS record for servers
# Устанавливаем запись
resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = "www.awsbootcamp.online"
  type    = "A"
  ttl     = "10"
  records = [aws_instance.eu_web_server.public_ip,
             aws_instance.asia_web_server.public_ip,
             aws_instance.us_web_server.public_ip]
}

*/