# Переменные загружаются из файла "terraform.tfvars"
# Общаие параметры проекта
variable "project_name" { type = string }
variable "project_code" {type = string }
variable "environment" { type = string }
variable "instance_type" { type = string }

/*
variable "project_tags" {
    type = "map"
    default = {
        Name = ""
        Terraform   = "true"
        Project     = var.project_name
        Code        = var.project_code
        Environment = var.environment
    }
}
*/

locals {
project_tags = {
        Terraform   = "true",
        Project     = var.project_name
        Code        = var.project_code
        Environment = var.environment
    }
}


# регион, в котором будет развернута конфигурация
variable "region" { default = "eu-west-1" }
variable "az" { default = [ "eu-west-1a", "eu-west-1b" ] }

# Приветствие сервера
variable "eu_greeting" { default = "Welcome from cloudy Ireland server !!!" }

# Образы серверов для разных регионов
variable "amis" {
  type = map
  default = {
    "eu-west-1" = "ami-04137ed1a354f54c4"
    "eu-west-2" = "ami-05c424d59413a2876"
    "ap-southeast-1" = "ami-093da183b859d5a4b"
    "us-east-1" = "ami-0dba2cb6798deb6d8"
  }
}

variable "ssh_security_key" { default = "aws-console-key" }
variable "ssh_public_key" {
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCflYYaVOn0PUAQYvd3D08UafXwpu3/7W0+0b5uO/idj/QwVd+M1h3EiC47CRGyNHITxRdD0jJERq+v1Bk+MCdAQTgzWHYG+yo0tBBMycQ2Dy6B3JuRX9MankIaGBBhFx/1nnZWLj5M5E9143RyHoZX/vUwdtjb3MNpfK8JdRRWhQlUvn9yneybPhykE4xxzUk58iNyIYA8h0zDoJ5p829jlMCjJ8vGY+M3NMtwEPu8dqldqS1zc10nwuP9NcikyJu9EldtTyZjYwXi8A5NSISHLrUgFuUMRCczp0IhAWtA5gUm9cj2SPOo7YCLRgq7KA/bfPgVngfG4AQBWPdGiotj"
    }

# HTML file name
variable "index_filename" { default = "index.html" }

